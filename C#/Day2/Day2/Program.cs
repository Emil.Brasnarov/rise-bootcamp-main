﻿
namespace KthElement;

public class Program
{   

    public static int KthMin(int k, int[] array)

    {
        Array.Sort(array);
        if(array.Length -1 >= k)
        {
            return array[k];
        } else { return 0; }
        
    }

    static void Main(string[] args)
    {
    }
}