﻿namespace FindMissing
{
    public class Program
    {
        //Bubble sort
        public static void SortArray(int[] NumArray)
        {
            var n = NumArray.Length;

            for (int i = 0; i < n - 1; i++)
                for (int j = 0; j < n - i - 1; j++)
                    if (NumArray[j] > NumArray[j + 1])
                    {
                        var tempVar = NumArray[j];
                        NumArray[j] = NumArray[j + 1];
                        NumArray[j + 1] = tempVar;
                    }
        }

        //Find if we have a number greater than the previous w/2
        public static int FindMissingNumber(int[] arr)
        {
            SortArray(arr);

            int[] answer = new int[1];



            for (int i = 1; i < arr.Length - 1; i++)
            {
                if (!(arr[i] == arr[i - 1]+1)) {
                    answer[0] = arr[i] -1;
                   };
            }

            if (answer[0] == 0)
            {
                return 0;
            }
            else
            {
            return answer[0];
            }
        }




        static void Main(string[] args)
        {
        }
    }
}