﻿using RemoveMid;

namespace RemoveMidTest
{
    internal class Program
    {

        [TestMethod]
        public void TestMethod1()
        {

            List<int> input = new List<int> { 4, 4, 4, 4, 2, 29, 0 };
            List<int> expected = new List<int> { 4, 2, 29, 0 };


            List<int> result = Program.UniqueElements(input);

            Assert.AreEqual(expected.Count(), result.Count());




        }


        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
        }
    }
}